package com.hibernate.modelo;

import javax.persistence.*;;

@Entity
@Table(name="modulo", uniqueConstraints=@UniqueConstraint(columnNames= {"Id"}))
public class Modulo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "Id", nullable=false, unique=true)
private int Id;
	
	@Column(name = "nombre")
private String nombre;
	
	@Column(name = "creditos")
private float creditos;

public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public float getCreditos() {
	return creditos;
}
public void setCreditos(float creditos) {
	this.creditos = creditos;
}
@Override
public String toString() {
	return "Modulo [Id=" + Id + ", nombre=" + nombre + ", creditos=" + creditos + "]";
}

}
