package com.hibernate.modelo;

import javax.persistence.*;

@Entity
@Table(name="direccion", uniqueConstraints=@UniqueConstraint(columnNames= {"Id"}))

public class Direccion {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "Id", nullable=false, unique=true)
private int Id;
private String calle;
private int numero;
private String poblacion;
private String provincia;


public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public String getCalle() {
	return calle;
}
public void setCalle(String calle) {
	this.calle = calle;
}
public int getNumero() {
	return numero;
}
public void setNumero(int numero) {
	this.numero = numero;
}
public String getPoblacion() {
	return poblacion;
}
public void setPoblacion(String poblacion) {
	this.poblacion = poblacion;
}
public String getProvincia() {
	return provincia;
}
public void setProvincia(String provincia) {
	this.provincia = provincia;
}
@Override
public String toString() {
	return "Direccion [Id=" + Id + ", calle=" + calle + ", numero=" + numero + ", poblacion=" + poblacion
			+ ", provincia=" + provincia + "]";
}


}
