package com.hibernate.modelo;

import javax.persistence.*;
@Entity
@Table(name="correo", uniqueConstraints=@UniqueConstraint(columnNames= {"Id"}))

public class Correo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "Id", nullable=false, unique=true)
private int Id;
private String direccion;
private String proveedor;
public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public String getDireccion() {
	return direccion;
}
public void setDireccion(String direccion) {
	this.direccion = direccion;
}
public String getProveedor() {
	return proveedor;
}
public void setProveedor(String proveedor) {
	this.proveedor = proveedor;
}
@Override
public String toString() {
	return "Correo [Id=" + Id + ", direccion=" + direccion + ", proveedor=" + proveedor + "]";
}

}
