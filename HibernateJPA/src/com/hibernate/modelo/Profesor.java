package com.hibernate.modelo;

import java.util.List;

import javax.persistence.*; 

@Entity
@Table(name="profesor", uniqueConstraints=@UniqueConstraint(columnNames= {"Id"}))

public class Profesor {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "Id", nullable=false, unique=true)
private int Id;
private String nombre;
private String ape1;
private String ape2;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="direccion")
private Direccion direccion;
    
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="modulo")
private Modulo modulo;
    
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="IdProfe", nullable=false, unique=true)
private List<Correo> correos;

public int getId() {
	return Id;
}

public void setId(int id) {
	Id = id;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getApe1() {
	return ape1;
}

public void setApe1(String ape1) {
	this.ape1 = ape1;
}

public String getApe2() {
	return ape2;
}

public void setApe2(String ape2) {
	this.ape2 = ape2;
}

public Direccion getDireccion() {
	return direccion;
}

public void setDireccion(Direccion direccion) {
	this.direccion = direccion;
}

public Modulo getModulo() {
	return modulo;
}

public void setModulo(Modulo modulo) {
	this.modulo = modulo;
}

public List<Correo> getCorreos() {
	return correos;
}

public void setCorreos(List<Correo> correos) {
	this.correos = correos;
}

@Override
public String toString() {
	return "Profesor [Id=" + Id + ", nombre=" + nombre + ", ape1=" + ape1 + ", ape2=" + ape2 + ", direccion="
			+ direccion + ", modulo=" + modulo + ", correos=" + correos + "]";
}


}


 