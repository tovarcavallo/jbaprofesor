package com.hibernate.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.hibernate.*;

import com.hibernate.modelo.Correo;
import com.hibernate.modelo.Direccion;
import com.hibernate.modelo.Modulo;
import com.hibernate.modelo.Profesor;
import com.hibernate.utilidades.Utilidades;
import controlador.ProfesorControlador;

public class Pruebas {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Session session = Utilidades.getSessionFactory().openSession();
	    boolean salir = false;
	    try {
	    while (salir==false) {
        	System.out.println("Tabla Profesor");
        	System.out.println("1. Insertar Profesor");
        	System.out.println("2. Delete Porfesor");
        	System.out.println("3. Listar Profesores");
        	System.out.println("4. Update Profesor");
        	System.out.println("5. Obtener Profesor");
        	System.out.println("6. Salir");

        	
        	int respuesta = sc.nextInt();
		ProfesorControlador pc = new ProfesorControlador();
		
		switch(respuesta) {
		case 1:
			Scanner sd = new Scanner(System.in);
			Direccion dir1 = new Direccion();
			Modulo mod1 = new Modulo();
			Correo corr1= new Correo();
			Profesor p1 = new Profesor();
			//direccion
			System.out.println("Ingresar Nombre de Calle");
			String calle = sd.nextLine();
			System.out.println("Ingresar Numero de Calle");
			String numero = sd.nextLine();
			int num = Integer.parseInt(numero);
			System.out.println("Ingresar Poblacion");
			String poblacion = sd.nextLine();
			System.out.println("Ingresar Provincia");
			String provincia = sd.nextLine();
			
			dir1.setCalle(calle);
			dir1.setNumero(num);
			dir1.setPoblacion(poblacion);
			dir1.setProvincia(provincia);
			
			//modulo
			System.out.println("Nombre de Modulo");
			String modulo =sd.nextLine();
			System.out.println("Float de Creditos");
            String creditos = sd.nextLine();
            float cre = Float.parseFloat(creditos);
            
            mod1.setNombre(modulo);
            mod1.setCreditos(cre);
			
            //correo
        	List <Correo> lista = new ArrayList<>();
        	boolean mas = true;
        	 while (mas==true) {
        		  System.out.println("Ingresar Direccion de Correo");
        		     String direccion = sd.nextLine();
                  System.out.println("Ingresar Proveedor de Correo");
                     String proovedor = sd.nextLine();
                     
                      corr1.setProveedor(proovedor);
                      corr1.setDireccion(direccion);
                      
                   lista.add(corr1);
              	  
              	  boolean otra = true;
              	  Scanner sr = new Scanner(System.in);
              	  while (otra==true) {
        		  System.out.println("Deseas Agregar otro Correo?  s/n");
        		  String res = sr.next();
        		  if (res.equalsIgnoreCase("n")) {
        			  otra = false;
        			  mas=false;
        		  }else if (res.equals("s")) {
        			  otra = false;
        			  mas=true;
        		  }else {
        			  System.err.println("No ha ingresado una respuesta valida");
        		  }
              	  }
              	  

              	  

        	  }
        	 
        	 //Profesor
   		  System.out.println("Ingresar nombre de Profesor");
   		  String nombre = sd.nextLine();
		  System.out.println("Ingresar 1� Apellido Profesor");
   		  String ape1 = sd.nextLine();
		  System.out.println("Ingresar 2� Apellido Profesor");
   		  String ape2 = sd.nextLine();

		  p1.setNombre(nombre);
		  p1.setApe1(ape1);
		  p1.setApe2(ape2);
		  p1.setDireccion(dir1);
		  p1.setModulo(mod1);
		  p1.setCorreos(lista);
		  
			Profesor salida = pc.InsertarProfesor(p1,session);
	        System.out.println("Salida Profesor: "+salida.getId());
		  
break;
		
		case 2:
			try {
			Scanner p = new Scanner (System.in);
			System.out.println("Ingresar id del Profesor que desea eliminar");
			int resp = p.nextInt();
			pc.DeleteProfesor(resp,session);
			}catch (Exception e) {
				System.err.println(e);
			}
			break;
		
		case 3:
			pc.ListarProfesores(session);
			break;
			
			


		case 4:
			try {
			Scanner ss = new Scanner(System.in);
			System.out.println("Ingresar id del Profesor");
			String dni = ss.nextLine();
			int id = Integer.parseInt(dni);
			Profesor profe = pc.ObtenerProfesor(id,session);
			System.out.println("�Que desea Actualizar?");
        	System.out.println("1. Datos de Profesor");
        	System.out.println("2. Datos de Direccion del Profesor");
        	System.out.println("3. Datos de Modulo del Profesor");
        	System.out.println("4. A�adir Correo");
        	int res = ss.nextInt();
        	Scanner sd1 = new Scanner(System.in);
        	switch(res){
        	case 1:
         		  System.out.println("Ingresar nombre de Profesor");
           		  nombre = sd1.nextLine();
        		  System.out.println("Ingresar 1� Apellido Profesor");
           		  ape1 = sd1.nextLine();
        		  System.out.println("Ingresar 2� Apellido Profesor");
           		  ape2 = sd1.nextLine();
           		  
        		  profe.setNombre(nombre);
        		  profe.setApe1(ape1);
        		  profe.setApe2(ape2);
     
        		  pc.ActualizaProfesor(profe,session);
        		  break;
        	
        	case 2:
        		System.out.println("Ingresar Nombre de Calle");
    			calle = sd1.nextLine();
    			System.out.println("Ingresar Numero de Calle");
    			numero = sd1.nextLine();
    			num = Integer.parseInt(numero);
    			System.out.println("Ingresar Poblacion");
    			poblacion = sd1.nextLine();
    			System.out.println("Ingresar Provincia");
    			provincia = sd1.nextLine();
    			Direccion dir11 = new Direccion();
    			dir11.setCalle(calle);
    			dir11.setNumero(num);
    			dir11.setPoblacion(poblacion);
    			dir11.setProvincia(provincia);
    			
    			profe.setDireccion(dir11);
    			
    			pc.ActualizaProfesor(profe,session);
    			break;
            case 3:
            	System.out.println("Nombre de Modulo");
    		    modulo =sd1.nextLine();
    			System.out.println("Float de Creditos");
                creditos = sd1.nextLine();
                cre = Float.parseFloat(creditos);
                Modulo mod11 = new Modulo();
                mod11.setNombre(modulo);
                mod11.setCreditos(cre);
                
                profe.setModulo(mod11);
                
                pc.ActualizaProfesor(profe,session);
                break;
             
            case 4:
            	lista = profe.getCorreos();
            	System.out.println("Ingresar Direccion de Correo");
   		        String direccion = sd1.nextLine();
                System.out.println("Ingresar Proveedor de Correo");
                String proovedor = sd1.nextLine();
                Correo corr11 = new Correo();
                 corr11.setProveedor(proovedor);
                 corr11.setDireccion(direccion);
                 
              lista.add(corr11);
              
              profe.setCorreos(lista);
              
              pc.ActualizaProfesor(profe,session);
              break;
            default:
            	System.err.println("Por favor, Ingresar una de las opciones propuestas");
            	break;

        	}
			}
				catch (Exception e) {
					System.err.println(e);
				}
			
			break;
			
			case 5:
				try {
					Scanner p = new Scanner (System.in);
					System.out.println("Ingresar id del Profesor que desea buscar");
					int resp = p.nextInt();
					System.out.println(pc.ObtenerProfesor(resp,session));
				}catch (Exception e) {
					System.err.println(e);
				}
				break;
			case 6:
				salir = true;
				break;
			default:
            	System.err.println("Por favor, Ingresar una de las opciones propuestas");
                break;




			
		}
	}

}catch (Exception e) {
	System.err.println(e);

}
	    session.close();
	}
}
