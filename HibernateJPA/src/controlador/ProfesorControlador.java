package controlador;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


import com.hibernate.modelo.Profesor;
import com.hibernate.utilidades.Utilidades;

public class ProfesorControlador {
	public static void ActualizaProfesor(Profesor profesor, Session sesion) {
		Session session = sesion;
        Transaction tx = null;
        try {
			tx=session.beginTransaction(); 
        

        session.update(profesor);

        session.getTransaction().commit();
        System.out.println("Profesor " + profesor.getId() + " - actualizado");
        }catch (Exception e) {
			if(tx==null) {
			tx.rollback();
			}e.printStackTrace();
		}
        

		
		
	}


	public static Profesor InsertarProfesor(Profesor profesor,Session sesion) {
		Session session = sesion;
		Transaction tx = null;
		try {
			tx=session.beginTransaction();
		    session.save(profesor);
		    session.getTransaction().commit();
			
		}catch (Exception e) {
			if(tx==null) {
			tx.rollback();
			}e.printStackTrace();
		}
		return profesor;

	}

	public static Profesor ObtenerProfesor(int id, Session sesion) {
		Profesor profesor=null;
		Session session = sesion;
		Transaction tx = null;
		try {
			tx=session.beginTransaction();
		    profesor = (Profesor) session.load(Profesor.class, id);
		    session.getTransaction().commit();
			
		}catch (Exception e) {
			if(tx==null) {
			tx.rollback();
			}e.printStackTrace();
		}
		return profesor;
	}
	
	public static void DeleteProfesor(int id,Session sesion) {
		Session session = sesion;
        Transaction tx = session.beginTransaction();
        Profesor d = (Profesor)session.load(Profesor.class, id);
        session.delete(d);
        session.getTransaction().commit();
		 System.out.println("Delete de Profesor Realizado con Exito");
	

	}
	
	public static void ListarProfesores(Session sesion) {
		 Session session = sesion;
		 Query<Profesor> query = session.createQuery("from Profesor");
		 List<Profesor> profesor = query.list();
		 for (Profesor profe : profesor) {
			 Profesor c = profe;
			 System.out.println(c.toString());
			 
		 }
		 
	}
}
